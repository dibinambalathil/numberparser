﻿using NumberParser.Entity;
using NumberParser.Services;
using System;
using System.IO;
using System.Text;

namespace NumberParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

           var str =  Console.ReadLine();
            Parser(str);
            
        }

        public static void Parser(string str)
        {
            
            var strArr = str.Split(' ');
            if (strArr.Length == 2)
            {

                try
                {
                    var formatType = strArr[1];



                    var numArr = strArr[0].Split(',');

                    int[] intArr = Array.ConvertAll<string, int>(numArr, int.Parse);

                    Array.Sort(intArr);
                    Array.Reverse(intArr);

                    ICreator obj = ParseCreator.creator(formatType);
                    if (obj != null)
                    {
                        var data = obj.GetFileData(intArr);
                        FileCreator(data);

                    }
                    else
                    {
                        Console.WriteLine("File type not supported!");
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw ex;

                }
            }
            else
            {
                Console.WriteLine(" Invalid data! ");
            }
            

        }

        public static string FileCreator(fileDetailsEntity entity)
        {
            string fileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //string fileName = System.Reflection.Assembly.GetExecutingAssembly().Location
            var dDrivePath = @"D:\";
            string status = "Failed...";
            fileName = dDrivePath + entity.fileName;

            try
            {

                if (Directory.Exists(dDrivePath))
                { 
                    // Check if file already exists. If yes, delete it.     
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }

                    // Create a new file     
                    using (FileStream fs = File.Create(fileName))
                    {
                        // Add some text to file    
                        Byte[] title = new UTF8Encoding(true).GetBytes(entity.data);
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes("");
                        fs.Write(author, 0, author.Length);
                    }

                    // Open the stream and read it back.    
                    using (StreamReader sr = File.OpenText(fileName))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(s);
                        }
                    }
                    status = "File created in D Drive";
            }
                else
                {
                    Console.WriteLine("Please Insert D Drive");
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
                throw Ex;
            }
            return status;
        }
    }
}
