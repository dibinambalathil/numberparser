﻿using NumberParser.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParser.Services
{
   public interface ICreator
    {
        fileDetailsEntity GetFileData(int[] arr);
    }
}
