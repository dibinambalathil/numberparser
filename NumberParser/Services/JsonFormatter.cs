﻿using Newtonsoft.Json;
using NumberParser.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParser.Services
{
  public  class JsonFormatter : ICreator
    {
        public fileDetailsEntity GetFileData(int[] arr)
        {
            var json = JsonConvert.SerializeObject(arr);
            var entity = new fileDetailsEntity();
            entity.fileName = "JSONFormatted.json";
            entity.data = json;
            return entity;
        }

        
    }
}
