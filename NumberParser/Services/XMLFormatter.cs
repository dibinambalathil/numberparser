﻿using Newtonsoft.Json;
using NumberParser.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace NumberParser.Services
{
    public class XMLFormatter : ICreator
    {
        public fileDetailsEntity GetFileData(int[] arr)
        {
            XDocument doc = new XDocument();
            doc.Add(new XElement("root", arr.Select(x => new XElement("item", x))));
            var entity = new fileDetailsEntity();
            entity.fileName = "XMLFormatted.xml";
            entity.data = doc.ToString();
            return entity;
        }

    }
    
}
