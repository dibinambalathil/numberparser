﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParser.Services
{
    class ParseCreator
    {

        public static ICreator creator(string type)
        {
            ICreator obj = null;
                if (type.ToLower() == "json")
            {
                obj = new JsonFormatter();
            }
                else if (type.ToLower() == "xml")
            {
                obj = new XMLFormatter();
            }
            else if (type.ToLower() == "txt")
            {
                obj = new TextFormatter();
            }
            return obj;
        }

    }
}
