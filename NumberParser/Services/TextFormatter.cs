﻿using NumberParser.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParser.Services
{
    public class TextFormatter : ICreator
    {
        public fileDetailsEntity GetFileData(int[] arr)
        {
          
            var entity = new fileDetailsEntity();
            entity.fileName = "TextFormatted.txt";
            entity.data = string.Join(" ", arr); ;
            return entity;
        }
    }
}
